# etch-a-sketch

Created with Javascript as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/web-development-101/lessons/etch-a-sketch-project). 

# Final Thoughts

Completing this project allowed me to review DOM manipulation and more adavanced Javascript features like Regular Expressions. 
When deciding the simplest way to display the sketch pad grid I reviewed CSS concepts like float/clear, inline-block, flexbox, and CSS Grid.